package com.example.messenger_buiduythao_1811505310441;

public class LineMessenger {
    private int avatar;
    private String name;
    private String message;
    private String time;
    private int status;

    public LineMessenger(int avatar, String name, String message, String time, int status) {
        this.avatar = avatar;
        this.name = name;
        this.message = message;
        this.time = time;
        this.status = status;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
