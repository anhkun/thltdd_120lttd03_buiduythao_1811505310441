package com.example.messenger_buiduythao_1811505310441;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LineMesssengerAdapter extends BaseAdapter {
    private Context context_messenger;
    private List<LineMessenger> lineMessengerList;
    private int layout_messenger;

    public LineMesssengerAdapter(Context context_messenger, List<LineMessenger> lineMessengerList, int layout_messenger) {
        this.context_messenger = context_messenger;
        this.lineMessengerList = lineMessengerList;
        this.layout_messenger = layout_messenger;
    }

    @Override
    public int getCount() {
        return lineMessengerList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private class ViewHolderMessenger{
        CircleImageView image_avatar,image_status;
        TextView txtName,txtMessage,txtTime;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolderMessenger viewHolderMessenger;
        if (view==null)
        {
            LayoutInflater inflater_messenger= (LayoutInflater) context_messenger.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater_messenger.inflate(layout_messenger,null);
            viewHolderMessenger=new ViewHolderMessenger();
            viewHolderMessenger.image_avatar=(CircleImageView) view.findViewById(R.id.line_avatar);
            viewHolderMessenger.txtName=(TextView) view.findViewById(R.id.line_name);
            viewHolderMessenger.txtMessage=(TextView) view.findViewById(R.id.line_message);
            viewHolderMessenger.txtTime=(TextView) view.findViewById(R.id.line_time);
            viewHolderMessenger.image_status=(CircleImageView) view.findViewById(R.id.line_status);
            view.setTag(viewHolderMessenger);
        }
        else
        {
            viewHolderMessenger= (ViewHolderMessenger) view.getTag();
        }
        LineMessenger lineMessenger=lineMessengerList.get(i);
        viewHolderMessenger.image_avatar.setImageResource(lineMessenger.getAvatar());
        viewHolderMessenger.txtName.setText(lineMessenger.getName());
        viewHolderMessenger.txtMessage.setText(lineMessenger.getMessage());
        viewHolderMessenger.txtTime.setText(lineMessenger.getTime());
        viewHolderMessenger.image_status.setImageResource(lineMessenger.getStatus());
        return view;
    }
}
