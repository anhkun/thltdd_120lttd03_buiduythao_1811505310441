package com.example.messenger_buiduythao_1811505310441;

public class LineStory {
    private int avatarStory;
    private  String nameStory;

    public LineStory(int avatarStory, String nameStory) {
        this.avatarStory = avatarStory;
        this.nameStory = nameStory;
    }

    public int getAvatarStory() {
        return avatarStory;
    }

    public void setAvatarStory(int avatarStory) {
        this.avatarStory = avatarStory;
    }

    public String getNameStory() {
        return nameStory;
    }

    public void setNameStory(String nameStory) {
        this.nameStory = nameStory;
    }
}
