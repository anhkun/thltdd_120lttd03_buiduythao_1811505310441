package com.example.messenger_buiduythao_1811505310441;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LineStoryAdapter extends RecyclerView.Adapter<LineStoryAdapter.MyViewHolder> {
    private Context context_story;
    private List<LineStory> lineStoryList;

    public LineStoryAdapter(Context context_story, List<LineStory> lineStoryList) {
        this.context_story = context_story;
        this.lineStoryList = lineStoryList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView image_avatarStory;
        TextView txtNameStory;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            image_avatarStory=(CircleImageView) itemView.findViewById(R.id.line_avatarStory);
            txtNameStory=(TextView) itemView.findViewById(R.id.line_nameStory);
        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view_story= LayoutInflater.from(context_story).inflate(R.layout.line_story,parent,false);

        return new MyViewHolder(view_story);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LineStory lineStory=lineStoryList.get(position);
        holder.image_avatarStory.setImageResource(lineStory.getAvatarStory());
        holder.txtNameStory.setText(lineStory.getNameStory());
    }

    @Override
    public int getItemCount() {
        return lineStoryList.size();
    }


}
