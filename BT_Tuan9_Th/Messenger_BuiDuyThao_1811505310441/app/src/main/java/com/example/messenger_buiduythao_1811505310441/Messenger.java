package com.example.messenger_buiduythao_1811505310441;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class Messenger extends Activity {
    //Messenger
    ListView listviewMessenger;
    ArrayList<LineMessenger> lineMessengerArrayList;
    LineMesssengerAdapter lineMesssengerAdapter;
    //Story
    RecyclerView recyclerViewStory;
    List<LineStory> lineStoryList;
    private int location=0;
    private int check=0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.messenger);
        //messenger
        mappingMessenger();
        //story
        mappingStory();
        registerForContextMenu(listviewMessenger);
        //Long
        listviewMessenger.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                location=i;
                return false;
            }
        });

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.context,menu);
        menu.setHeaderIcon(R.drawable.messenger);
        menu.setHeaderTitle("Menu Context");

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.menu_them:
                check=1;
                DialogUpdate();
                break;
            case R.id.menu_sua:
                check=2;
                DialogUpdate();
                break;
            case R.id.menu_xoa:
                Delete(location);
                break;
        }
        return super.onContextItemSelected(item);
    }
    private void Delete(final int position){
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
        alertDialog.setTitle("Thông Báo !!!");
        alertDialog.setIcon(R.drawable.ic_launcher_foreground);
        alertDialog.setMessage("Can you want delete line "+lineMessengerArrayList.get(position).getName()+" in listview ?");
        alertDialog.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                lineMessengerArrayList.remove(position);
                lineMesssengerAdapter.notifyDataSetChanged();
                Toast.makeText(Messenger.this,"Bạn đã xóa thành công !!",Toast.LENGTH_LONG).show();
            }
        });
        alertDialog.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        alertDialog.show();
    }
    private  void mappingStory(){
        recyclerViewStory=(RecyclerView) findViewById(R.id.listviewStory);
        lineStoryList=new ArrayList<>();
        lineStoryList.add(new LineStory(R.drawable.videocamera,"Tạo phòng họp mặt"));
        lineStoryList.add(new LineStory(R.drawable.anh1,"Bình Triệu"));
        lineStoryList.add(new LineStory(R.drawable.anh2,"Duy Nguyễn"));
        lineStoryList.add(new LineStory(R.drawable.anh3,"Uyên Hoàng"));
        lineStoryList.add(new LineStory(R.drawable.anh4,"Minh Hoàng"));
        lineStoryList.add(new LineStory(R.drawable.anh5,"Thảo Nguyên"));
        lineStoryList.add(new LineStory(R.drawable.anh6,"Nhi Pi"));
        lineStoryList.add(new LineStory(R.drawable.anh7,"Dũng"));
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewStory.setLayoutManager(linearLayoutManager);

        LineStoryAdapter lineStoryAdapter=new LineStoryAdapter(this,lineStoryList);
        recyclerViewStory.setAdapter(lineStoryAdapter);
    }
    private  void mappingMessenger(){
        listviewMessenger=(ListView) findViewById(R.id.listviewMessenger);
        lineMessengerArrayList=new ArrayList<>();
        lineMessengerArrayList.add(new LineMessenger(R.drawable.anh9,"Nguyên Nguyễn","Bạn:Ăn cơm chưa","22:22",R.drawable.tick));
        lineMessengerArrayList.add(new LineMessenger(R.drawable.anh8,"Lâm Hạnh Tiên","Ăn rồi","22:22",R.drawable.black));
        lineMessengerArrayList.add(new LineMessenger(R.drawable.anh7,"Dũng","Bạn:Đi chơi không ?","22:22",R.drawable.anh7));
        lineMessengerArrayList.add(new LineMessenger(R.drawable.anh6,"Nhi Pi","Ok !!","22:22",R.drawable.black));
        lineMessengerArrayList.add(new LineMessenger(R.drawable.anh5,"Thảo Nguyên","Bạn:Hôm nay trời rất...","22:22",R.drawable.checked));
        lineMessengerArrayList.add(new LineMessenger(R.drawable.anh4,"Minh Hoàng","Bạn:Ăn cơm chưa alo...","22:22",R.drawable.checked));
        lineMessengerArrayList.add(new LineMessenger(R.drawable.anh3,"Uyên Hoàng","Bạn:Ok !!","22:22",R.drawable.tick));
        lineMessengerArrayList.add(new LineMessenger(R.drawable.anh2,"Duy Nguyễn","Ok.Mai học","22:22",R.drawable.black));
        lineMessengerArrayList.add(new LineMessenger(R.drawable.anh1,"Bình Triệu","Bạn:Đi chơi nào !!","22:22",R.drawable.anh1));

        lineMesssengerAdapter=new LineMesssengerAdapter(this,lineMessengerArrayList,R.layout.line_messenger);
        listviewMessenger.setAdapter(lineMesssengerAdapter);
    }
    private  void DialogUpdate(){
        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.editmessenger);
        dialog.setCanceledOnTouchOutside(true);
        final EditText inputName=(EditText) dialog.findViewById(R.id.inputName);
        final EditText inputMessage=(EditText) dialog.findViewById(R.id.inputMessage);
        final EditText inputTime=(EditText) dialog.findViewById(R.id.inputTime);
        Button btnOk=(Button) dialog.findViewById(R.id.btnQL);
        Button btnHuy=(Button) dialog.findViewById(R.id.btnHuy);
        if(check==2)
        {
            inputName.setText(lineMessengerArrayList.get(location).getName());
            inputMessage.setText(lineMessengerArrayList.get(location).getMessage());
            inputTime.setText(lineMessengerArrayList.get(location).getTime());
        }
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(check==1) {
                    lineMessengerArrayList.add(new LineMessenger(R.drawable.ic_launcher_foreground, inputName.getText().toString(), inputMessage.getText().toString(), inputTime.getText().toString(), R.drawable.black));
                    lineMesssengerAdapter.notifyDataSetChanged();
                    dialog.cancel();
                }
                else {
                    lineMessengerArrayList.get(location).setName(inputName.getText().toString());
                    lineMessengerArrayList.get(location).setMessage(inputMessage.getText().toString());
                    lineMessengerArrayList.get(location).setTime(inputTime.getText().toString());
                    lineMesssengerAdapter.notifyDataSetChanged();
                    dialog.cancel();
                }
            }
        });
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        dialog.show();
        }
    }

