package com.example.btth10_1_buiduythao_1811505310441;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PeopleAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<People> peopleList;

    public PeopleAdapter(Context context, int layout, List<People> peopleList) {
        this.context = context;
        this.layout = layout;
        this.peopleList = peopleList;
    }

    @Override
    public int getCount() {
        return peopleList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private class ViewHolder{
        CircleImageView image_avatar;
        ImageView image_star;
        TextView txtName,txtAddress,txtPhone;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view==null)
        {
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holder=new ViewHolder();
            holder.image_avatar=(CircleImageView) view.findViewById(R.id.image_avatar);
            holder.image_star=(ImageView) view.findViewById(R.id.image_star);
            holder.txtName=(TextView) view.findViewById(R.id.txtName);
            holder.txtAddress=(TextView) view.findViewById(R.id.txtAddress);
            holder.txtPhone=(TextView) view.findViewById(R.id.txtPhone);
            view.setTag(holder);
        }
        else
            holder= (ViewHolder) view.getTag();
        People people=peopleList.get(i);
        holder.image_avatar.setImageResource(people.getAvatar());
        holder.image_star.setImageResource(people.getStar());
        holder.txtName.setText(people.getName());
        holder.txtAddress.setText(people.getAddress());
        holder.txtPhone.setText(people.getPhone());
        return view;
    }
}
