package com.example.btth10_1_buiduythao_1811505310441;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    ListView listView;
    ArrayList<People> arrayList;
    PeopleAdapter adapter,adapter1;
    ImageButton btnGrid,btnList;
    GestureDetector gestureDetector;
    private List<People> Userselection=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnGrid=(ImageButton) findViewById(R.id.btnGrid);
        btnList=(ImageButton) findViewById(R.id.btnList);
        mapping();
        gridView.setVisibility(View.GONE);
        ///
        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(modeListener);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showMenu();
            }
        });
        //
        gridView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        gridView.setMultiChoiceModeListener(modeListener);
        //
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listView.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.GONE);
                btnGrid.setBackgroundColor(getResources().getColor(R.color.list));
                btnList.setBackgroundColor(getResources().getColor(R.color.grid));
            }
        });
        btnGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gridView.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                btnList.setBackgroundColor(getResources().getColor(R.color.list));
                btnGrid.setBackgroundColor(getResources().getColor(R.color.grid));
            }
        });
        gestureDetector=new GestureDetector(this,new Mygesture());
        listView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                gestureDetector.onTouchEvent(motionEvent);
                return true;
            }
        });
    }
    AbsListView.MultiChoiceModeListener modeListener=new AbsListView.MultiChoiceModeListener() {
        @Override
        public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {
            if(Userselection.contains(arrayList.get(i)))
                Userselection.remove(arrayList.get(i));
            else
                Userselection.add(arrayList.get(i));
            actionMode.setTitle(Userselection.size()+" item selected...");
        }

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater menuInflater=actionMode.getMenuInflater();
            menuInflater.inflate(R.menu.acsionmode,menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId())
            {
                case R.id.menu_delete:
                    DialogDelete(actionMode);
                    return true;
                default:return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            Userselection.clear();
        }
    };
    public void DialogDelete(final ActionMode actionMode){
        AlertDialog.Builder dialogDelete=new AlertDialog.Builder(this);
        dialogDelete.setMessage("Bạn có muốn xóa danh sách đã chọn này không !!");
        dialogDelete.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                for(int j=0;j<Userselection.size();j++)
                {
                    arrayList.remove(Userselection.get(j));
                }
                adapter.notifyDataSetChanged();
                adapter1.notifyDataSetChanged();
                actionMode.finish();
                Toast.makeText(MainActivity.this,"Xóa thành công !!",Toast.LENGTH_LONG).show();
            }
        });
        dialogDelete.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                actionMode.finish();
            }
        });
        dialogDelete.show();
    }
    private void mapping(){
        gridView=(GridView) findViewById(R.id.gridviewPeople);
        listView=(ListView) findViewById(R.id.listviewPeople);
        arrayList=new ArrayList<>();
        arrayList.add(new People(R.drawable.anhdaidien,R.drawable.star,"Bùi Duy Thảo","Đà Nẵng","0941780740"));
        arrayList.add(new People(R.drawable.anh1,R.drawable.star,"Triệu Tân Bình","Đà Nẵng","0941780740"));
        arrayList.add(new People(R.drawable.anh2,R.drawable.star,"Nguyễn Thanh Duy","Đà Nẵng","0941780740"));
        arrayList.add(new People(R.drawable.anh3,R.drawable.star,"Hoàng Thu Uyên","Đà Nẵng","0941780740"));
        arrayList.add(new People(R.drawable.anh4,R.drawable.star,"Hoàng Minh","Đà Nẵng","0941780740"));
        arrayList.add(new People(R.drawable.anh5,R.drawable.star,"Thảo Nguyên","Đà Nẵng","0941780740"));
        arrayList.add(new People(R.drawable.anh8,R.drawable.star,"Lâm Hạnh Tiên","Đà Nẵng","0941780740"));
        arrayList.add(new People(R.drawable.anh7,R.drawable.star,"Dũng","Đà Nẵng","0941780740"));
        arrayList.add(new People(R.drawable.anh9,R.drawable.star,"Nguyên Nguyễn","Đà Nẵng","0941780740"));
        arrayList.add(new People(R.drawable.anh6,R.drawable.star,"Tiểu Nhi","Đà Nẵng","0941780740"));
        adapter=new PeopleAdapter(MainActivity.this,R.layout.line_people,arrayList);
        gridView.setAdapter(adapter);
        adapter1=new PeopleAdapter(MainActivity.this,R.layout.line_peoplelist,arrayList);
        listView.setAdapter(adapter1);
    }
    private void showMenu(){
        PopupMenu popupMenu=new PopupMenu(this,listView);
        popupMenu.getMenuInflater().inflate(R.menu.manager,popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId())
                {
                    case R.id.menu_New:
                        break;
                    case R.id.menu_Edit:
                        Toast.makeText(MainActivity.this,"Update",Toast.LENGTH_LONG).show();
                        break;
                    case R.id.menu_DeleteOne:
                        Toast.makeText(MainActivity.this,"Delete",Toast.LENGTH_LONG).show();
                        break;
                }
                return false;
            }
        });
        popupMenu.show();

    }
    class Mygesture extends  GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if((e2.getY()-e1.getY())>100)
            {
                Toast.makeText(MainActivity.this,"scroll xuống",Toast.LENGTH_LONG).show();
            }
            return super.onScroll(e1, e2, distanceX, distanceY);
        }
    }
}