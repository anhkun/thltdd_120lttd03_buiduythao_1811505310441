package com.example.btth10_1_buiduythao_1811505310441;

public class People {
    private int avatar;
    private int star;
    private String name;
    private String address;
    private String phone;

    public People(int avatar, int star, String name, String address, String phone) {
        this.avatar = avatar;
        this.star = star;
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
