package com.example.tuan7_buiduythao_1811505310441;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView tv_signup;
    Button btn_login;
    EditText input_Name;
    EditText input_Pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_signup=(TextView) findViewById(R.id.tv_signup);
        btn_login=(Button) findViewById(R.id.btn_login);
        input_Name=(EditText) findViewById(R.id.input_user);
        input_Pass=(EditText) findViewById(R.id.input_pass);
        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signup=new Intent(MainActivity.this,Signup.class);
                startActivity(signup);
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_result=getIntent();
                Bundle bundle_result=intent_result.getBundleExtra("Account");
                if (input_Name.getText().toString().equalsIgnoreCase(bundle_result.getString("Name")) &&
                input_Pass.getText().toString().equalsIgnoreCase(bundle_result.getString("Pass")))
                {
                    Intent customlist=new Intent(MainActivity.this,Customlist.class);
                    startActivity(customlist);
                }
                else
                    Toast.makeText(MainActivity.this,"Wrong username or password!!",Toast.LENGTH_LONG).show();
            }
        });

    }
}