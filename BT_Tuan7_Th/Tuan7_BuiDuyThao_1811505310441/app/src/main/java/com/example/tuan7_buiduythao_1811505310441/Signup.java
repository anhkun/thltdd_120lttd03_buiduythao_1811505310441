package com.example.tuan7_buiduythao_1811505310441;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class Signup extends Activity {
    TextView tv_login;
    EditText inputName;
    EditText inputEmail;
    EditText inputPass;
    Button btnSignup;
    private String name;
    private String pass;
    private Boolean signUp=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        tv_login=(TextView) findViewById(R.id.tv_login);
        inputName=(EditText) findViewById(R.id.input_user);
        inputEmail=(EditText) findViewById(R.id.input_email);
        inputPass=(EditText) findViewById(R.id.input_pass);
        btnSignup=(Button) findViewById(R.id.btn_signup);
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login=new Intent(Signup.this,MainActivity.class);
                if(signUp==false)
                    startActivity(login);
                else {
                    Bundle bundle=new Bundle();
                    bundle.putString("Name",name);
                    bundle.putString("Pass",pass);
                    login.putExtra("Account",bundle);
                    startActivity(login);
                }
            }
        });
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name=inputName.getText().toString();
                pass=inputPass.getText().toString();
                if (name==""||pass==""||inputEmail.getText().toString()=="")
                    Toast.makeText(Signup.this,"You can not to empty!!",Toast.LENGTH_LONG).show();
                else{
                    Toast.makeText(Signup.this,"Sign up success!!",Toast.LENGTH_LONG).show();
                    signUp=true;
                }
            }
        });
    }
}
