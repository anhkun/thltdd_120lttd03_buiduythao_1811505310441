package com.example.tuan7_buiduythao_1811505310441;

public class Information {
    private String title_information;
    private String content_information;
    private int picture_information;

    public Information(String title_information, String content_information, int picture_information) {
        this.title_information = title_information;
        this.content_information = content_information;
        this.picture_information = picture_information;
    }

    public String getTitle_information() {
        return title_information;
    }

    public void setTitle_information(String title_information) {
        this.title_information = title_information;
    }

    public String getContent_information() {
        return content_information;
    }

    public void setContent_information(String content_information) {
        this.content_information = content_information;
    }

    public int getPicture_information() {
        return picture_information;
    }

    public void setPicture_information(int picture_information) {
        this.picture_information = picture_information;
    }
}
