package com.example.tuan7_buiduythao_1811505310441;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class Customlist extends Activity {
    ListView lvOnepiece;
    ArrayList<Onepiece> onepieceArrayList;
    OnepieceAdapter onepieceAdapter;
    TextView txtprofile;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customlist);
        txtprofile=(TextView) findViewById(R.id.tv_cqprofile);
        txtprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent profile=new Intent(Customlist.this,Profile.class);
                startActivity(profile);
            }
        });
        anhxa();
        onepieceAdapter=new OnepieceAdapter(this,R.layout.onepiece,onepieceArrayList);
        lvOnepiece.setAdapter(onepieceAdapter);

    }
    private void anhxa(){
        lvOnepiece=(ListView) findViewById(R.id.listviewonepiece);
        onepieceArrayList=new ArrayList<>();
        onepieceArrayList.add(new Onepiece("Luffy","1.500.000.000$",R.drawable.luffy));
        onepieceArrayList.add(new Onepiece("Zoro","320.000.000$",R.drawable.zoro));
        onepieceArrayList.add(new Onepiece("Chopper","100.000$",R.drawable.chopper));
        onepieceArrayList.add(new Onepiece("Ace","500.000.000$",R.drawable.ace));
        onepieceArrayList.add(new Onepiece("Sabo","700.000.000$",R.drawable.sabo));
    }
}
