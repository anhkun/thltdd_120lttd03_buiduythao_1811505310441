package com.example.tuan7_buiduythao_1811505310441;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class OnepieceAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Onepiece> onepieceList;

    public OnepieceAdapter(Context context, int layout, List<Onepiece> onepieceList) {
        this.context = context;
        this.layout = layout;
        this.onepieceList = onepieceList;
    }

    @Override
    public int getCount() {
        return onepieceList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view=inflater.inflate(layout,null);
        TextView txtName_onepiece=(TextView) view.findViewById(R.id.tvname_onepiece);
        TextView txtPrice_onepiece=(TextView) view.findViewById(R.id.tvprice_onepiece);
        ImageView image_onepiece=(ImageView) view.findViewById(R.id.image_onepiece);
        Onepiece onepiece=onepieceList.get(i);
        txtName_onepiece.setText(onepiece.getName());
        txtPrice_onepiece.setText(onepiece.getPrice());
        image_onepiece.setImageResource(onepiece.getPicture());
        return view;
    }
}
