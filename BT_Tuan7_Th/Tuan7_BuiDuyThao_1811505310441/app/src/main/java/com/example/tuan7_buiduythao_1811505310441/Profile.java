package com.example.tuan7_buiduythao_1811505310441;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class Profile extends Activity {
    ListView listview_information;
    ArrayList<Information> informationArrayList;
    InformationAdapter informationAdapter;
    Button btnLogout;
    TextView txtLogout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        txtLogout=(TextView) findViewById(R.id.tv_Logout);
        txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login=new Intent(Profile.this,MainActivity.class);
                startActivity(login);
            }
        });
        btnLogout=(Button) findViewById(R.id.btn_logout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        anhxa();
        informationAdapter=new InformationAdapter(this, informationArrayList, R.layout.information);
        listview_information.setAdapter(informationAdapter);


    }
    private void anhxa(){
        listview_information=(ListView) findViewById(R.id.listviewinformation);
        informationArrayList =new ArrayList<>();
        informationArrayList.add(new Information("Use name","Bùi Duy Thảo",R.drawable.users));
        informationArrayList.add(new Information("Phone","01699257287",R.drawable.phone));
        informationArrayList.add(new Information("Email","bdt2103@gmail.com",R.drawable.email1));
        informationArrayList.add(new Information("Dress","K162/31 Đống Đa",R.drawable.map));
    }
}
