package com.example.tuan7_buiduythao_1811505310441;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class InformationAdapter extends BaseAdapter {
    private Context context;
    private List<Information> informationList;
    private  int layout;

    public InformationAdapter(Context context, List<Information> informationList, int layout) {
        this.context = context;
        this.informationList = informationList;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return informationList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view=inflater.inflate(layout,null);
        TextView txtTitle_information=(TextView) view.findViewById(R.id.tvtitle_information);
        TextView txtContent_information=(TextView) view.findViewById(R.id.tvcontent_information);
        ImageView image_information=(ImageView) view.findViewById(R.id.image_information);
        Information information=informationList.get(i);
        txtTitle_information.setText(information.getTitle_information());
        txtContent_information.setText(information.getContent_information());
        image_information.setImageResource(information.getPicture_information());
        return view;
    }
}
