package com.example.baithicuoi;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class FixProductGioHang extends AppCompatActivity {
    TextView txtName,txtNote,txtPrice,txtSLTD,txtTotalPrice,btnXong,btnCong,btnTru;
    ImageView image;
    ImageButton btnClose;
    RadioButton RbtnM,RbtnL;
    int TotalPrice=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fixproductgiohang);
        mapping();
        Intent intent_result=getIntent();
        Bundle bundle_result=intent_result.getBundleExtra("Product");
        txtName.setText(bundle_result.getString("name"));
        txtSLTD.setText(bundle_result.getString("qty"));
        txtPrice.setText(bundle_result.getString("price"));
        txtNote.setText(bundle_result.getString("note"));
        Picasso.with(FixProductGioHang.this).load(bundle_result.getString("image"))
                .placeholder(R.drawable.anhdaidien)
                .error(R.drawable.ic_baseline_close_24)
                .into(image);
        if(bundle_result.getString("size").equals("Size M"))
            RbtnM.setChecked(true);
        else
            RbtnL.setChecked(true);
        if(RbtnM.isChecked())
            TotalPrice=Integer.parseInt(txtSLTD.getText().toString())*Integer.parseInt(txtPrice.getText().toString());
        else
            TotalPrice=Integer.parseInt(txtSLTD.getText().toString())*(Integer.parseInt(txtPrice.getText().toString())+10000);
        txtTotalPrice.setText(TotalPrice+"đ");
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnCong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int SLTD=Integer.parseInt(txtSLTD.getText().toString())+1;
                txtSLTD.setText(""+SLTD);
                if(RbtnL.isChecked())
                {
                    int total=SLTD*(Integer.parseInt(txtPrice.getText().toString())+10000);
                    txtTotalPrice.setText(total+"đ");
                }
                else
                {
                    int total=SLTD*Integer.parseInt(txtPrice.getText().toString());
                    txtTotalPrice.setText(total+"đ");
                }
            }
        });
        btnTru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int SLTD=Integer.parseInt(txtSLTD.getText().toString());
                if(SLTD==1)
                {
                    String id=String.valueOf(bundle_result.getInt("id"));
                    String url="http://192.168.43.147:8080/Android/Passio%20Coffee/delete.php";
                    DeleteProductGH(url,id,bundle_result.getString("name"));
                }
                else SLTD--;
                txtSLTD.setText(""+SLTD);
                if(RbtnL.isChecked())
                {
                    int total=SLTD*(Integer.parseInt(txtPrice.getText().toString())+10000);
                    txtTotalPrice.setText(total+"đ");
                }
                else
                {
                    int total=SLTD*Integer.parseInt(txtPrice.getText().toString());
                    txtTotalPrice.setText(total+"đ");
                }
            }
        });
        RbtnL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int SLTD=Integer.parseInt(txtSLTD.getText().toString());
                int total=SLTD*(Integer.parseInt(txtPrice.getText().toString())+10000);
                txtTotalPrice.setText(total+"đ");
            }
        });
        RbtnM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int SLTD=Integer.parseInt(txtSLTD.getText().toString());
                int total=SLTD*Integer.parseInt(txtPrice.getText().toString());
                txtTotalPrice.setText(total+"đ");
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnXong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id=String.valueOf(bundle_result.getInt("id"));
                String url="http://192.168.43.147:8080/Android/Passio%20Coffee/update.php";
                String name=txtName.getText().toString();
                String icon=bundle_result.getString("image");
                String size="";
                if(RbtnM.isChecked())
                {
                    size="Size M";
                }
                else
                {
                    size="Size L";
                }
                String qty=txtSLTD.getText().toString();
                String price=txtPrice.getText().toString();
                String note=txtNote.getText().toString();
                String idSP=String.valueOf(bundle_result.getInt("idSP"));
                UpdateGioHang(url,id,name,icon,size,qty,price,note,idSP);
                startActivity(new Intent(FixProductGioHang.this,GioHang.class));
            }
        });
    }
    private  void mapping(){
        txtName=(TextView) findViewById(R.id.txtName_fixproduct_giohang);
        txtNote=(TextView) findViewById(R.id.txtNote__fixproduct_giohang);
        txtPrice=(TextView) findViewById(R.id.txtPrice__fixproduct_giohang);
        txtSLTD=(TextView) findViewById(R.id.txtSLTD__fixproduct_giohang);
        txtTotalPrice=(TextView) findViewById(R.id.txtTotalPrice_fixproduct_giohang);
        btnXong=(TextView) findViewById(R.id.txtXong_fixproduct_giohang);
        btnTru=(TextView) findViewById(R.id.btntru_fixproduct_giohang);
        btnCong=(TextView) findViewById(R.id.btncong_fixproduct_giohang);
        image=(ImageView) findViewById(R.id.image__fixproduct_giohang);
        btnClose=(ImageButton) findViewById(R.id.btnClose_fixproduct_giohang);
        RbtnM=(RadioButton) findViewById(R.id.RbtnSizeM__fixproduct_giohang);
        RbtnL=(RadioButton) findViewById(R.id.RbtnSizeL__fixproduct_giohang);
    }
    private  void DeletePGH(String url,String id) {
        RequestQueue requestQueue=Volley.newRequestQueue(FixProductGioHang.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equals("success"))
                            Toast.makeText(FixProductGioHang.this,"Delete thành công",Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(FixProductGioHang.this,"Delete thất bại",Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(FixProductGioHang.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("idPGH",id);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void UpdateGioHang(String url,String id,String name,String icon,String size,String sl,String price,String note,String idSP){
        RequestQueue requestQueue=Volley.newRequestQueue(FixProductGioHang.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equals("success"))
                            Toast.makeText(FixProductGioHang.this,"Update thành công",Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(FixProductGioHang.this,"Update thất bại",Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(FixProductGioHang.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("name",name);
                parems.put("icon",icon);
                parems.put("size",size);
                parems.put("sl",sl);
                parems.put("price",price);
                parems.put("note",note);
                parems.put("idSP",idSP);
                parems.put("id",id);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void DeleteProductGH(String url,String id,String name){
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
        alertDialog.setTitle("Thông Báo !!!");
        alertDialog.setIcon(R.drawable.ic_launcher_foreground);
        alertDialog.setMessage("Bạn có chắc xóa "+ name+" này khỏi giỏ hàng không ?");
        alertDialog.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DeletePGH(url,id);
                startActivity(new Intent(FixProductGioHang.this,GioHang.class));
            }
        });
        alertDialog.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        alertDialog.show();
    }
}