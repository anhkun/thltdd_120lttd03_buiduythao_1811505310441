package com.example.baithicuoi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class Pay extends AppCompatActivity {
    TextView btnYup;
    RadioButton RbtnTM,RbtnATM;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);
        Toolbar toolbar=findViewById(R.id.toolbar_pay);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_close_24);
        btnYup=(TextView) findViewById(R.id.btnYup_Pay);
        RbtnATM=(RadioButton) findViewById(R.id.RbtnPayATM_pay);
        RbtnTM=(RadioButton) findViewById(R.id.RbtnPayTM_pay);
        RbtnTM.setChecked(true);
        btnYup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url="http://192.168.43.147:8080/Android/Passio%20Coffee/up_pay_detail.php";
                if(RbtnTM.isChecked()) UpdateAddressDetail(url,String.valueOf(0));
                else UpdateAddressDetail(url,String.valueOf(1));
                startActivity(new Intent(Pay.this,GioHang.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
    private void UpdateAddressDetail(String url,String pay){
        RequestQueue requestQueue= Volley.newRequestQueue(Pay.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Pay.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("pay",pay);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
}