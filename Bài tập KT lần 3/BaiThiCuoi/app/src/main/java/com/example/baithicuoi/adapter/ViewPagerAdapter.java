package com.example.baithicuoi.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.baithicuoi.fragment.FreshFragment;
import com.example.baithicuoi.fragment.IceFragment;
import com.example.baithicuoi.fragment.PassioFragment;
import com.example.baithicuoi.fragment.TeaFragment;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:return new IceFragment();
            case 1:return new TeaFragment();
            case 2:return new PassioFragment();
            case 3:return new FreshFragment();
            default:return new IceFragment();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title="";
        switch (position){
            case 0:title="Ice blended";
                break;
            case 1:title="Tea/soda";
                break;
            case 2:title="Passio coffee";
                break;
            case 3:title="Fresh & easy";
                break;
        }
        return title;
    }
}
