package com.example.baithicuoi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Detail_TinTuc extends AppCompatActivity {
    TextView txtTitle,txtContent;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail__tin_tuc);
        //
       Toolbar toolbar=findViewById(R.id.toolbar_detailnews);
       setSupportActionBar(toolbar);
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        //
        txtTitle=findViewById(R.id.txtTitle_DetailNews);
        txtContent=findViewById(R.id.txtContent_DetailNews);
        imageView=(ImageView) findViewById(R.id.image_detailtintuc);
        Intent intent_result=getIntent();
        Bundle bundle_result=intent_result.getBundleExtra("News");
        txtTitle.setText(bundle_result.getString("title"));
        txtContent.setText(bundle_result.getString("content"));
        Picasso.with(Detail_TinTuc.this).load(bundle_result.getString("image"))
                .placeholder(R.drawable.anhdaidien)
                .into(imageView);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}