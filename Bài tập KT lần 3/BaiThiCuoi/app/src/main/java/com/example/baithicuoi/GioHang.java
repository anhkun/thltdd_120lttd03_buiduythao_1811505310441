package com.example.baithicuoi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.baithicuoi.adapter.ProductGioHangAdapter;
import com.example.baithicuoi.model.ProductGioHang;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class GioHang extends AppCompatActivity {
    ExpandableHeightListView listView;
    ArrayList<ProductGioHang> array;
    ProductGioHangAdapter adapter;
    LinearLayout layoutSale,layoutNote,layoutAddress;
    RelativeLayout layoutDathang;
    RelativeLayout layoutpayATM,layoutpayTM;
    TextView txtTotalQty,txtTotalPriceTT,txtTotalPrice,txtTotalPriceDH,txtNote,txtSale,txtAddress;
    int Qty=0,PriceTT=0,Price=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gio_hang);
        Toolbar toolbar=findViewById(R.id.toolbar_giohang);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        mapping();
        String urlgiohang="http://192.168.43.147:8080/Android/Passio%20Coffee/product_giohang.php";
        SelectGioHang(urlgiohang);
        String urlAddressDetail="http://192.168.43.147:8080/Android/Passio%20Coffee/detail_address.php";
        SelectAddressDetail(urlAddressDetail);
        layoutpayATM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GioHang.this,Pay.class));
            }
        });
        layoutpayTM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GioHang.this,Pay.class));
            }
        });
        layoutSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GioHang.this,FixCodeDecreased.class));
            }
        });
        layoutNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GioHang.this,FixNoteGioHang.class));
            }
        });
        layoutAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GioHang.this,Address.class));
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int id=array.get(i).getId();
                String name=array.get(i).getName();
                String note=array.get(i).getNote();
                String image=array.get(i).getImage();
                String qty=array.get(i).getSl();
                String price=array.get(i).getPrice();
                String size=array.get(i).getSize();
                int idSP=array.get(i).getIdSP();
                Intent intent=new Intent(GioHang.this,FixProductGioHang.class);
                Bundle bundle=new Bundle();
                bundle.putInt("id",id);
                bundle.putString("name",name);
                bundle.putString("note",note);
                bundle.putString("image",image);
                bundle.putString("qty",qty);
                bundle.putString("price",price);
                bundle.putString("size",size);
                bundle.putInt("idSP",idSP);
                intent.putExtra("Product",bundle);
                startActivity(intent);
            }
        });
        layoutDathang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogDatHang();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            startActivity(new Intent(GioHang.this,ProductList.class));
        return super.onOptionsItemSelected(item);
    }

    private void mapping(){
        layoutDathang=(RelativeLayout) findViewById(R.id.LayoutDatHang_GioHang);
        layoutpayATM=(RelativeLayout) findViewById(R.id.LayoutPayATM_giohang);
        layoutpayTM=(RelativeLayout) findViewById(R.id.LayoutPayTM_giohang);
        txtAddress=(TextView) findViewById(R.id.txtAddress_giohang);
        layoutAddress=(LinearLayout) findViewById(R.id.layoutAddress_GioHang);
        layoutSale=(LinearLayout) findViewById(R.id.LayoutSale_GioHang);
        layoutNote=(LinearLayout) findViewById(R.id.LayoutNote_GioHang);
        txtTotalQty=(TextView) findViewById(R.id.txtTotalQty_giohang);
        txtTotalPriceTT=(TextView) findViewById(R.id.txtTotalPriceTT_giohang);
        txtTotalPrice=(TextView) findViewById(R.id.txtTotalPrice_giohang);
        txtTotalPriceDH=(TextView) findViewById(R.id.txtToTalPriceDH_giohang);
        txtNote=(TextView) findViewById(R.id.txtNote_giohang);
        txtSale=(TextView) findViewById(R.id.txtSale_giohang);
        listView=(ExpandableHeightListView) findViewById(R.id.listviewGioHang_product);
        array=new ArrayList<>();
        adapter=new ProductGioHangAdapter(GioHang.this,array,R.layout.line_product_giohang);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }
    private void SelectGioHang(String url) {
        RequestQueue requestQueue= Volley.newRequestQueue(GioHang.this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Qty=0;
                        PriceTT=0;
                        for(int i=0;i<response.length();i++)
                        {
                            try {
                                JSONObject object=response.getJSONObject(i);
                                array.add(new ProductGioHang(object.getInt("id"),object.getString("name"),object.getString("icon"),object.getString("size"),String.valueOf(object.getInt("sl")),String.valueOf(object.getInt("price")),object.getString("note"),object.getInt("idSP")));
                                Qty++;
                                if(object.getString("size").equals("Size M"))
                                    PriceTT+=object.getInt("sl")*object.getInt("price");
                                else
                                    PriceTT+=object.getInt("sl")*(object.getInt("price")+10000);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                        DecimalFormat format=new DecimalFormat("###,###.###");
                        txtTotalQty.setText("DANH SÁCH MÓN ("+Qty+")");
                        txtTotalPriceTT.setText(format.format(PriceTT)+"đ");
                        txtTotalPrice.setText(format.format(PriceTT-5000)+"đ");
                        txtTotalPriceDH.setText(format.format(PriceTT-5000)+"đ");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(GioHang.this,error.toString(),Toast.LENGTH_SHORT).show();
            }
        }
        );
        requestQueue.add(jsonArrayRequest);
    }
    private void SelectAddressDetail(String url) {
        RequestQueue requestQueue= Volley.newRequestQueue(GioHang.this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject object=response.getJSONObject(0);
                            txtAddress.setText(object.getString("address"));
                            txtSale.setText(object.getString("codesale"));
                            txtNote.setText(object.getString("note"));
                            if(object.getInt("pay")==0)
                            {
                                layoutpayTM.setVisibility(View.VISIBLE);
                                layoutpayATM.setVisibility(View.GONE);
                            }
                            else
                            {
                                layoutpayTM.setVisibility(View.GONE);
                                layoutpayATM.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(GioHang.this,error.toString(),Toast.LENGTH_SHORT).show();
            }
        }
        );
        requestQueue.add(jsonArrayRequest);
    }
    private void DialogDatHang(){
        View dialogSheetview= LayoutInflater.from(GioHang.this).inflate(R.layout.dialog_dathang,null);
        BottomSheetDialog dialog=new BottomSheetDialog(GioHang.this);
        dialog.setContentView(dialogSheetview);
        dialog.setCanceledOnTouchOutside(false);
        SeekBar seekBar=(SeekBar) dialogSheetview.findViewById(R.id.seekBar_dialogDatHang);
        TextView btnNext=(TextView) dialogSheetview.findViewById(R.id.btnNext_dialogDatHang);
        seekBar.setEnabled(false);
        MediaPlayer mediaPlayer=MediaPlayer.create(GioHang.this,R.raw.nhac);
        mediaPlayer.start();
        CountDownTimer countDownTimer=new CountDownTimer(60000,50) {
            @Override
            public void onTick(long l) {
                if(seekBar.getProgress()==100)
                    seekBar.setProgress(0);
                seekBar.setProgress(seekBar.getProgress() + 1);
            }
            @Override
            public void onFinish() {
                this.start();
            }
        };
        countDownTimer.start();
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url="http://192.168.43.147:8080/Android/Passio%20Coffee/deleteallgiohang.php";
                DeleteAllGioHang(url);
                mediaPlayer.pause();
                dialog.cancel();
                startActivity(new Intent(GioHang.this,ProductList.class));
            }
        });
        dialog.show();
    }
    private  void DeleteAllGioHang(String url){
        RequestQueue requestQueue= Volley.newRequestQueue(GioHang.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(GioHang.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                });
        requestQueue.add(stringRequest);
    }
}