package com.example.baithicuoi.model;

public class Product {
    private int id_product;
    private String image_product;
    private String name_product;
    private String price_product;
    private String note_product;

    public Product(int id_product, String image_product, String name_product, String price_product, String note_product) {
        this.id_product = id_product;
        this.image_product = image_product;
        this.name_product = name_product;
        this.price_product = price_product;
        this.note_product = note_product;
    }

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public String getImage_product() {
        return image_product;
    }

    public void setImage_product(String image_product) {
        this.image_product = image_product;
    }

    public String getName_product() {
        return name_product;
    }

    public void setName_product(String name_product) {
        this.name_product = name_product;
    }

    public String getPrice_product() {
        return price_product;
    }

    public void setPrice_product(String price_product) {
        this.price_product = price_product;
    }

    public String getNote_product() {
        return note_product;
    }

    public void setNote_product(String note_product) {
        this.note_product = note_product;
    }
}
