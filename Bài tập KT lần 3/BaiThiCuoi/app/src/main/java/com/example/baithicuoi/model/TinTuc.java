package com.example.baithicuoi.model;

public class TinTuc {
    private int id;
    private String picture;
    private String tittle;
    private String content;

    public TinTuc(int id, String picture, String tittle, String content) {
        this.id = id;
        this.picture = picture;
        this.tittle = tittle;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
