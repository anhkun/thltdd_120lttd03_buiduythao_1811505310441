package com.example.baithicuoi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.baithicuoi.R;
import com.example.baithicuoi.model.TinTuc;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TinTucAdapter extends BaseAdapter {
    private Context context;
    private List<TinTuc> list;
    private int layout;

    public TinTucAdapter(Context context, List<TinTuc> list, int layout) {
        this.context = context;
        this.list = list;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private  class ViewlHolder{
        ImageView imageView;
        TextView txtTitle,txtContent;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewlHolder holder;
        if(view==null)
        {
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holder=new ViewlHolder();
            holder.imageView=(ImageView) view.findViewById(R.id.imageTinTuc);
            holder.txtTitle=(TextView) view.findViewById(R.id.txtTitleTinTuc);
            holder.txtContent=(TextView) view.findViewById(R.id.txtContentTintuc);
            view.setTag(holder);
        }
        else
            holder= (ViewlHolder) view.getTag();
        TinTuc tinTuc=list.get(i);
        Picasso.with(context).load(tinTuc.getPicture())
                .placeholder(R.drawable.anhdaidien)
                .error(R.drawable.ic_baseline_close_24)
                .into(holder.imageView);
        holder.txtTitle.setText(tinTuc.getTittle());
        holder.txtContent.setText(tinTuc.getContent());
        return view;
    }
}
