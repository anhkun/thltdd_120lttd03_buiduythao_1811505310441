package com.example.baithicuoi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.baithicuoi.adapter.TinTucAdapter;
import com.example.baithicuoi.model.TinTuc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class Home extends AppCompatActivity {
    ExpandableHeightListView listView;
    ArrayList<TinTuc> array;
    TinTucAdapter adapter;
    ImageButton btnProductList,btnNotification,btnCoupon;
    CircleImageView btnProfile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mapping();
        String url="http://192.168.43.147:8080/Android/Passio%20Coffee/news.php";
        selectTinTuc(url);
        btnProductList=(ImageButton) findViewById(R.id.btnProductList);
        btnProductList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this,ProductList.class));
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String title=array.get(i).getTittle();
                String content=array.get(i).getContent();
                String image=array.get(i).getPicture();
                Intent intent=new Intent(Home.this,Detail_TinTuc.class);
                Bundle bundle=new Bundle();
                bundle.putString("image",image);
                bundle.putString("title",title);
                bundle.putString("content",content);
                intent.putExtra("News",bundle);
                startActivity(intent);
            }
        });
        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this,Profile.class));
            }
        });
        btnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this,Notification.class));
            }
        });
        btnCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this,Coupon.class));
            }
        });
    }
    private void mapping(){
        btnCoupon=(ImageButton) findViewById(R.id.btnCoupon_Home);
        btnNotification=(ImageButton) findViewById(R.id.btnNotification_Home);
        btnProfile=(CircleImageView) findViewById(R.id.btnavatar);
        listView=(ExpandableHeightListView) findViewById(R.id.listviewHome);
        array=new ArrayList<>();
        adapter=new TinTucAdapter(Home.this,array,R.layout.line_tintuc);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }
    private void selectTinTuc(String url)
    {
        RequestQueue requestQueue= Volley.newRequestQueue(Home.this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i=0;i<response.length();i++)
                        {
                            try {
                                JSONObject object=response.getJSONObject(i);
                                array.add(new TinTuc(object.getInt("id"),object.getString("picture"),object.getString("title"),object.getString("note")));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Home.this,error.toString()+"",Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(jsonArrayRequest);
    }
}