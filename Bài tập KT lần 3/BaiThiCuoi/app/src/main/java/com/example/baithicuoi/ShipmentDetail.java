package com.example.baithicuoi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.HashMap;
import java.util.Map;

public class ShipmentDetail extends AppCompatActivity {
    TextView txtPlace,btnLuu;
    EditText inputAddress,inputName,inputTel;
    ImageButton btnClose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipment_detail);
        txtPlace=(TextView) findViewById(R.id.txtPlace_Shipment);
        inputAddress=(EditText) findViewById(R.id.inputAddress_shipment);
        inputName=(EditText) findViewById(R.id.inputName_shipment);
        inputTel=(EditText) findViewById(R.id.inputTel_shipment);
        btnLuu=(TextView) findViewById(R.id.txtLuu_shipment);
        btnClose=(ImageButton) findViewById(R.id.btnClose_shipment);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txtPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogPlace();
            }
        });

        btnLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url="http://192.168.43.147:8080/Android/Passio%20Coffee/insert_address.php";
                String address="Địa chỉ: "+inputAddress.getText().toString();
                String name="Người nhận: "+inputName.getText().toString();
                String tel="Số điện thoại: "+inputTel.getText().toString();
                if(!address.equals("") && !name.equals("") && !tel.equals(""))
                {
                    InsertAddress(url,"Địa điểm: "+txtPlace.getText().toString(),address,name,tel);
                    startActivity(new Intent(ShipmentDetail.this,Address.class));
                }
                else
                    Toast.makeText(ShipmentDetail.this,"Không được để trống !!",Toast.LENGTH_LONG).show();
            }
        });
    }
    private  void DialogPlace(){
        View dialogSheetview= LayoutInflater.from(ShipmentDetail.this).inflate(R.layout.dialog_place_address,null);
        BottomSheetDialog dialog=new BottomSheetDialog(ShipmentDetail.this);
        dialog.setContentView(dialogSheetview);
        TextView txtCoquan=(TextView) dialogSheetview.findViewById(R.id.txtCoquan_dialog_shipment);
        TextView txtNha=(TextView) dialogSheetview.findViewById(R.id.txtNha_dialog_shipment);
        TextView txtKhac=(TextView) dialogSheetview.findViewById(R.id.txtKhac_dialog_shipment);
        ImageButton btnClose=(ImageButton) dialogSheetview.findViewById(R.id.btnClose_dialog_shipment);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        txtCoquan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtPlace.setText(txtCoquan.getText().toString());
                dialog.cancel();
            }
        });
        txtNha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtPlace.setText(txtNha.getText().toString());
                dialog.cancel();
            }
        });
        txtKhac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtPlace.setText(txtKhac.getText().toString());
                dialog.cancel();
            }
        });
        dialog.show();
    }
    private void InsertAddress(String url,String place,String address,String name,String tel)
    {
        RequestQueue requestQueue= Volley.newRequestQueue(ShipmentDetail.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ShipmentDetail.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("place",place);
                parems.put("address",address);
                parems.put("name",name);
                parems.put("tel",tel);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
}