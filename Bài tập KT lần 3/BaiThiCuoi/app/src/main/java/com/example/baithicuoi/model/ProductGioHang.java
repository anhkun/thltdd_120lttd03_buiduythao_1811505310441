package com.example.baithicuoi.model;

public class ProductGioHang {
    private int id;
    private String name;
    private String image;
    private String size;
    private String sl;
    private String price;
    private String note;
    private int idSP;

    public ProductGioHang(int id, String name, String image, String size, String sl, String price, String note, int idSP) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.size = size;
        this.sl = sl;
        this.price = price;
        this.note = note;
        this.idSP = idSP;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getIdSP() {
        return idSP;
    }

    public void setIdSP(int idSP) {
        this.idSP = idSP;
    }
}
