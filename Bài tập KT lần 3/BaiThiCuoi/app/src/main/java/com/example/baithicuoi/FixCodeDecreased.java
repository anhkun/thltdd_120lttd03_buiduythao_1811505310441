package com.example.baithicuoi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class FixCodeDecreased extends AppCompatActivity {
    TextView btnXong;
    ImageButton btnClose;
    EditText inputCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fixcodedecreased);
        mapping();
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnXong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url="http://192.168.43.147:8080/Android/Passio%20Coffee/up_codesale_detail.php";
                UpdateAddressDetail(url,inputCode.getText().toString());
                startActivity(new Intent(FixCodeDecreased.this,GioHang.class));
            }
        });
    }
    private void mapping(){
        btnXong=(TextView) findViewById(R.id.txtXong_fixSale_giohang);
        btnClose=(ImageButton) findViewById(R.id.btnClose_fixsale_giohang);
        inputCode=(EditText) findViewById(R.id.inputMaSale_fixSaleGioHang);
    }
    private void UpdateAddressDetail(String url,String codesale){
        RequestQueue requestQueue= Volley.newRequestQueue(FixCodeDecreased.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(FixCodeDecreased.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("codesale",codesale);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
}