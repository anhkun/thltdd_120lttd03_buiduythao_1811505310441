package com.example.baithicuoi.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.baithicuoi.GioHang;
import com.example.baithicuoi.R;
import com.example.baithicuoi.adapter.ProductAdapter;
import com.example.baithicuoi.model.Product;
import com.example.baithicuoi.model.ProductGioHang;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link IceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IceFragment extends Fragment {
    GridView gridView;
    ArrayList<Product> array;
    ArrayList<ProductGioHang> arrayGiohang;
    ProductAdapter adapter;
    private View mRootView;
    String url;
    private RelativeLayout layout;
    int total=0,Tonggia=0,Qty=0;
    String urlgiohang="http://192.168.43.147:8080/Android/Passio%20Coffee/product_giohang.php";
    TextView txtTolalMon,txtToTalPrice;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public IceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment IceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static IceFragment newInstance(String param1, String param2) {
        IceFragment fragment = new IceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView=inflater.inflate(R.layout.fragment_ice, container, false);
        mapping();
        layout.setVisibility(View.GONE);
        url="http://192.168.43.147:8080/Android/Passio%20Coffee/product_ice.php";
        getData(url);
        SelectGioHang(urlgiohang);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Dialog1(i);
            }
        });
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), GioHang.class));
            }
        });
        // Inflate the layout for this fragment
        return mRootView;
    }
    private void mapping(){
        layout=(RelativeLayout) mRootView.findViewById(R.id.Layout_ChuyenGioHang);
        gridView=mRootView.findViewById(R.id.gridView_Ice);
        txtTolalMon=(TextView) mRootView.findViewById(R.id.txtSL_giohang_productlist) ;
        txtToTalPrice=(TextView) mRootView.findViewById(R.id.txtprice_giohang_productlist);
        array=new ArrayList<>();
        arrayGiohang=new ArrayList<>();
        adapter=new ProductAdapter(getContext(),array,R.layout.line_product);
        gridView.setAdapter(adapter);
    }
    private void Dialog1(int position){
        View dialogSheetview= LayoutInflater.from(getContext()).inflate(R.layout.dialog_themvaogiohang,null);
        BottomSheetDialog dialog=new BottomSheetDialog(getContext());
        dialog.setContentView(dialogSheetview);
        RelativeLayout btnlayout=(RelativeLayout) dialogSheetview.findViewById(R.id.btnLayout_ThemGioHang);
        ImageView imageView=(ImageView) dialogSheetview.findViewById(R.id.image_dialog_themvaogiohang);
        TextView txtName=(TextView) dialogSheetview.findViewById(R.id.txtName_dialog_themvaogiohang);
        TextView txtPrice=(TextView) dialogSheetview.findViewById(R.id.txtPrice_dialog_themvaogiohang);
        TextView txtNote=(TextView) dialogSheetview.findViewById(R.id.txtNote_dialog_themvaogiohang);
        TextView txtSLTD=(TextView) dialogSheetview.findViewById(R.id.txtSLTD_dialog_themvaogiohang);
        TextView btnCong=(TextView) dialogSheetview.findViewById(R.id.btncong_dialog_themvaogiohang);
        TextView btnTru=(TextView) dialogSheetview.findViewById(R.id.btntru_dialog_themvaogiohang);
        TextView txtTotal=(TextView) dialogSheetview.findViewById(R.id.txtTotalprice_dialog_themvaogiohang);
        ImageButton btnClose=(ImageButton) dialogSheetview.findViewById(R.id.btnClose_dialog_themvaogiohang);
        RadioButton btnSizeL=(RadioButton) dialogSheetview.findViewById(R.id.RbtnSizeL_dialog_themvaogiohang);
        RadioButton btnSizeM=(RadioButton) dialogSheetview.findViewById(R.id.RbtnSizeM_dialog_themvaogiohang);
        btnSizeM.setChecked(true);
        btnSizeM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int SLTD=Integer.parseInt(txtSLTD.getText().toString());
                total=SLTD*Integer.parseInt(txtPrice.getText().toString());
                txtTotal.setText(total+"đ");
            }
        });
        btnSizeL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int SLTD=Integer.parseInt(txtSLTD.getText().toString());
                total=SLTD*(Integer.parseInt(txtPrice.getText().toString())+10000);
                txtTotal.setText(total+"đ");
            }
        });
        txtName.setText(array.get(position).getName_product());
        txtPrice.setText(array.get(position).getPrice_product());
        txtNote.setText(array.get(position).getNote_product());
        txtTotal.setText(txtPrice.getText()+"đ");
        //Tang giam SL;
        btnCong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int SLTD=Integer.parseInt(txtSLTD.getText().toString())+1;
                txtSLTD.setText(""+SLTD);
                if(btnSizeL.isChecked())
                {
                    total=SLTD*(Integer.parseInt(txtPrice.getText().toString())+10000);
                    txtTotal.setText(total+"đ");
                }
                else
                {
                    total=SLTD*Integer.parseInt(txtPrice.getText().toString());
                    txtTotal.setText(total+"đ");
                }

            }
        });
        btnTru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int SLTD=Integer.parseInt(txtSLTD.getText().toString());
                if( SLTD !=1) SLTD--;
                txtSLTD.setText(""+SLTD);
                if(btnSizeL.isChecked())
                {
                    total=SLTD*(Integer.parseInt(txtPrice.getText().toString())+10000);
                    txtTotal.setText(total+"đ");
                }
                else
                {
                    total=SLTD*Integer.parseInt(txtPrice.getText().toString());
                    txtTotal.setText(total+"đ");
                }
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        //load hinh anh
        Picasso.with(getContext()).load(array.get(position).getImage_product())
                .placeholder(R.drawable.anhdaidien)
                .error(R.drawable.ic_baseline_close_24)
                .into(imageView);
        //them vao gio hang
        btnlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //kiem tra
                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String urlupdate="http://192.168.43.147:8080/Android/Passio%20Coffee/update.php";
                        String urlinsert="http://192.168.43.147:8080/Android/Passio%20Coffee/insert.php";
                        String name=array.get(position).getName_product();
                        String icon=array.get(position).getImage_product();
                        String size="";
                        if(btnSizeM.isChecked()) size="Size M";
                        else size="Size L";
                        String sl=txtSLTD.getText().toString();
                        String price=array.get(position).getPrice_product();
                        String note=array.get(position).getNote_product();
                        int idProduct=array.get(position).getId_product();
                        int ssltotaltrunglap=Integer.parseInt(txtSLTD.getText().toString());
                        int dktd=0;
                        if(arrayGiohang.size()!=0)
                        {
                            for(int j=0;j<arrayGiohang.size();j++)
                            {
                                if(idProduct==arrayGiohang.get(j).getIdSP())
                                {
                                    if(size.equals(arrayGiohang.get(j).getSize()))
                                    {
                                        dktd=1;
                                        String idGioHang=String.valueOf(arrayGiohang.get(j).getId());
                                        ssltotaltrunglap=ssltotaltrunglap+Integer.parseInt(arrayGiohang.get(j).getSl());
                                        UpdateGioHang(urlupdate,idGioHang,name,icon,size,String.valueOf(ssltotaltrunglap),price,note,String.valueOf(idProduct));
                                        arrayGiohang.clear();
                                        break;
                                    }
                                }
                            }
                            if(dktd==0) insertGioHang(urlinsert,name,icon,size,sl,price,note,String.valueOf(idProduct));
                        }
                        else insertGioHang(urlinsert,name,icon,size,sl,price,note,String.valueOf(idProduct));
                    }
                },800);
                Handler handler1=new Handler();
                handler1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        SelectGioHang(urlgiohang);
                    }
                },1200);
                dialog.cancel();
            }
        });
        dialog.show();
    }
    private void getData(String url) {
        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i=0;i<response.length();i++)
                        {
                            try {
                                JSONObject object=response.getJSONObject(i);
                                array.add(new Product(object.getInt("id"),
                                        object.getString("icon"),
                                        object.getString("name"),
                                        String.valueOf(object.getInt("price")),
                                        object.getString("note")));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),error.toString(),Toast.LENGTH_SHORT).show();
            }
        }
        );
        requestQueue.add(jsonArrayRequest);
    }
    private void insertGioHang(String url,String name,String icon,String size,String sl,String price,String note,String idSP){
        RequestQueue requestQueue=Volley.newRequestQueue(getContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("name",name);
                parems.put("icon",icon);
                parems.put("size",size);
                parems.put("sl",sl);
                parems.put("price",price);
                parems.put("note",note);
                parems.put("idSP",idSP);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void UpdateGioHang(String url,String id,String name,String icon,String size,String sl,String price,String note,String idSP){
        RequestQueue requestQueue=Volley.newRequestQueue(getContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("name",name);
                parems.put("icon",icon);
                parems.put("size",size);
                parems.put("sl",sl);
                parems.put("price",price);
                parems.put("note",note);
                parems.put("idSP",idSP);
                parems.put("id",id);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void SelectGioHang(String url) {
        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Qty=0;
                        Tonggia=0;
                        for(int i=0;i<response.length();i++)
                        {
                            try {
                                Qty++;
                                JSONObject object=response.getJSONObject(i);
                                arrayGiohang.add(new ProductGioHang(
                                        object.getInt("id"),
                                        object.getString("name"),
                                        object.getString("icon"),
                                        object.getString("size"),
                                        String.valueOf(object.getInt("sl")),
                                        String.valueOf(object.getInt("price")),
                                        object.getString("note"),
                                        object.getInt("idSP")));
                                if(object.getString("size").equals("Size M"))
                                    Tonggia+=object.getInt("sl")*object.getInt("price");
                                else
                                    Tonggia+=object.getInt("sl")*(object.getInt("price")+10000);
                                layout.setVisibility(View.VISIBLE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        DecimalFormat format=new DecimalFormat("###,###.###");
                        txtTolalMon.setText(Qty+" món");
                        txtToTalPrice.setText(format.format(Tonggia)+"đ");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),error.toString(),Toast.LENGTH_SHORT).show();
            }
        }
        );
        requestQueue.add(jsonArrayRequest);
    }
}