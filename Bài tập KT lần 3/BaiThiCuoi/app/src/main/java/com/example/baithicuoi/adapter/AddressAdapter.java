package com.example.baithicuoi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.baithicuoi.R;
import com.example.baithicuoi.model.Address;

import java.util.List;

public class AddressAdapter extends BaseAdapter {
    private Context context;
    private List<Address> list;
    private int layout;

    public AddressAdapter(Context context, List<Address> list, int layout) {
        this.context = context;
        this.list = list;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private class ViewHolder{
        TextView txtPlace,txtAddress,txtName,txtTel;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view==null)
        {
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holder=new ViewHolder();
            holder.txtPlace=(TextView) view.findViewById(R.id.txtPlace_lineAddress);
            holder.txtAddress=(TextView) view.findViewById(R.id.txtAddress_lineAddress);
            holder.txtName=(TextView) view.findViewById(R.id.txtName_lineAddress);
            holder.txtTel=(TextView) view.findViewById(R.id.txtTel_lineAddress);
            view.setTag(holder);
        }
        else
            holder= (ViewHolder) view.getTag();
        Address address=list.get(i);
        holder.txtPlace.setText(address.getPlace());
        holder.txtAddress.setText(address.getAddress());
        holder.txtName.setText(address.getName());
        holder.txtTel.setText(address.getTel());
        return view;
    }
}
