package com.example.baithicuoi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.baithicuoi.model.ProductGioHang;
import com.example.baithicuoi.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductGioHangAdapter extends BaseAdapter {
    private Context context;
    private List<ProductGioHang> list;
    private int layout;

    public ProductGioHangAdapter(Context context, List<ProductGioHang> list, int layout) {
        this.context = context;
        this.list = list;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private class ViewHolder{
        ImageView imageView;
        TextView txtName,txtSize,txtSL,txtPrice;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view==null)
        {
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holder=new ViewHolder();
            holder.imageView=(ImageView) view.findViewById(R.id.imageproduct_giohang);
            holder.txtName=(TextView) view.findViewById(R.id.txtNameproduct_giohang);
            holder.txtSize=(TextView) view.findViewById(R.id.txtSizeproduct_giohang);
            holder.txtSL=(TextView) view.findViewById(R.id.txtSLproduct_giohang);
            holder.txtPrice=(TextView) view.findViewById(R.id.txtPriceproduct_giohang);
            view.setTag(holder);
        }
        else
            holder= (ViewHolder) view.getTag();
        ProductGioHang productGioHang=list.get(i);
        Picasso.with(context).load(productGioHang.getImage())
                .placeholder(R.drawable.anhdaidien)
                .error(R.drawable.ic_baseline_close_24)
                .into(holder.imageView);
        holder.txtName.setText(productGioHang.getName());
        holder.txtSize.setText(productGioHang.getSize());
        holder.txtSL.setText(productGioHang.getSl());
        holder.txtPrice.setText(productGioHang.getPrice());
        return view;
    }
}
