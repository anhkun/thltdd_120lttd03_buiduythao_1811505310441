package com.example.baithicuoi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.baithicuoi.adapter.AddressAdapter;
import com.example.baithicuoi.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Address extends AppCompatActivity {
    TextView btnThemmoi;
    ExpandableHeightListView listView;
    ArrayList<com.example.baithicuoi.model.Address> array;
    AddressAdapter adapter;
    ProgressBar progressBar;
    int kt=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        mapping();
        Toolbar toolbar=findViewById(R.id.toolbar_Address);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        btnThemmoi.setVisibility(View.GONE);
        listView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        btnThemmoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Address.this,ShipmentDetail.class));
            }
        });
        String url="http://192.168.43.147:8080/Android/Passio%20Coffee/address.php";
        getAddress(url);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String address=array.get(i).getPlace()+"\n"+array.get(i).getAddress()+"\n"+array.get(i).getName()+"\n"+array.get(i).getTel();
                String url="http://192.168.43.147:8080/Android/Passio%20Coffee/up_address_detail.php";
                UpdateAddressDetail(url,address);
                startActivity(new Intent(Address.this,GioHang.class));
            }
        });
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                if(kt!=0)
                {
                    btnThemmoi.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                }
                else
                {
                    listView.setVisibility(View.GONE);
                    btnThemmoi.setVisibility(View.VISIBLE);
                }
            }
        },3000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_address,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                startActivity(new Intent(Address.this,GioHang.class));
                break;
            case R.id.menu__newplace:
                startActivity(new Intent(Address.this,ShipmentDetail.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    private void mapping(){
        progressBar=(ProgressBar) findViewById(R.id.ProgressBar_Address);
        btnThemmoi=(TextView) findViewById(R.id.btnThemmoi_Address);
        listView=(ExpandableHeightListView) findViewById(R.id.listview_Address);
        array=new ArrayList<>();
        adapter=new AddressAdapter(Address.this,array,R.layout.line_address);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }
    private void getAddress(String url) {
        RequestQueue requestQueue= Volley.newRequestQueue(Address.this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if(response.length()!=0) kt=1;
                        for(int i=0;i<response.length();i++)
                        {
                            try {
                                JSONObject object=response.getJSONObject(i);
                                array.add(new com.example.baithicuoi.model.Address(object.getInt("id"),object.getString("place"),object.getString("address"),object.getString("name"),object.getString("tel")));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Address.this,error.toString(),Toast.LENGTH_SHORT).show();
            }
        }
        );
        requestQueue.add(jsonArrayRequest);
    }
    private void UpdateAddressDetail(String url,String address){
        RequestQueue requestQueue= Volley.newRequestQueue(Address.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Address.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("address",address);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
}