package com.example.baithicuoi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.baithicuoi.model.Product;
import com.example.baithicuoi.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends BaseAdapter {
   private Context context;
   private List<Product> List;
    private int layout;

    public ProductAdapter(Context context, java.util.List<Product> list, int layout) {
        this.context = context;
        List = list;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return List.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private  class ViewHolder{
        ImageView image;
        TextView txtName,txtPrice;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view==null)
        {
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holder=new ViewHolder();
            holder.image=(ImageView) view.findViewById(R.id.image_product);
            holder.txtName=(TextView) view.findViewById(R.id.txtName_product);
            holder.txtPrice=(TextView) view.findViewById(R.id.txtPrice_product);
            view.setTag(holder);
        }
        else
            holder= (ViewHolder) view.getTag();
        Product product=List.get(i);
        Picasso.with(context).load(product.getImage_product())
                .placeholder(R.drawable.anhdaidien)
                .error(R.drawable.ic_baseline_close_24)
                .into(holder.image);
        holder.txtName.setText(product.getName_product());
        holder.txtPrice.setText(product.getPrice_product());
        return view;
    }

}
