package com.example.baithicuoi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class FixNoteGioHang extends AppCompatActivity {
    TextView btnXong;
    EditText inputNote;
    ImageButton btnClose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fixnotegiohang);
        mapping();
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnXong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url="http://192.168.43.147:8080/Android/Passio%20Coffee/up_note_detail.php";
                UpdateAddressDetail(url,inputNote.getText().toString());
                startActivity(new Intent(FixNoteGioHang.this,GioHang.class));
            }
        });
    }
    private void mapping(){
        btnClose=(ImageButton) findViewById(R.id.btnClose_fixnote_giohang);
        btnXong=(TextView) findViewById(R.id.txtXong_fixNote_giohang);
        inputNote=(EditText) findViewById(R.id.inputNote_fixNoteGioHang);
    }
    private void UpdateAddressDetail(String url,String note){
        RequestQueue requestQueue= Volley.newRequestQueue(FixNoteGioHang.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(FixNoteGioHang.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("note",note);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
}