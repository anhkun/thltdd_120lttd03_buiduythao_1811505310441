package com.example.activityqualai;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Layout1 extends Activity {
    Button btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout1);
        btnBack=(Button) findViewById(R.id.btnback);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                /*
                Intent callme=new Intent(Intent.ACTION_CALL);
                callme.setData(Uri.parse("tel:0941314137"));
                startActivity(callme);
                */
            }
        });
    }
}
