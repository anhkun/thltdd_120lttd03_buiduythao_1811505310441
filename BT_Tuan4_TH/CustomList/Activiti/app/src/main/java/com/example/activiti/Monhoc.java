package com.example.activiti;

public class Monhoc {
    private  String monHoc;
    private String tinChi;
    private int anh;

    public Monhoc(String monHoc, String tinChi, int anh) {
        this.monHoc = monHoc;
        this.tinChi = tinChi;
        this.anh = anh;
    }

    public String getMonHoc() {
        return monHoc;
    }

    public void setMonHoc(String monHoc) {
        this.monHoc = monHoc;
    }

    public String getTinChi() {
        return tinChi;
    }

    public void setTinChi(String tinChi) {
        this.tinChi = tinChi;
    }

    public int getAnh() {
        return anh;
    }

    public void setAnh(int anh) {
        this.anh = anh;
    }
}
