package com.example.activiti;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MonhocAdapter extends BaseAdapter {
    private Context context;
    private  int layout;
    private List<Monhoc> monHocList;

    public MonhocAdapter(Context context, int layout, List<Monhoc> monHocList) {
        this.context = context;
        this.layout = layout;
        this.monHocList = monHocList;
    }

    @Override
    public int getCount() {
        return monHocList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view=inflater.inflate(layout,null);
        //anh xa
        TextView txtMon=(TextView) view.findViewById(R.id.txtMon);
        TextView txtsotinchi=(TextView) view.findViewById(R.id.txtTinchi);
        ImageView imageAnh=(ImageView) view.findViewById(R.id.imageMon);
        //gán giá trị
        Monhoc monHoc=monHocList.get(i);

        txtMon.setText(monHoc.getMonHoc());
        txtsotinchi.setText(monHoc.getTinChi());
        imageAnh.setImageResource(monHoc.getAnh());
        return view;
    }
}
