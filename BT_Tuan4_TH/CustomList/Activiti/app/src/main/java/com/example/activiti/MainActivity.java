package com.example.activiti;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.time.Instant;

public class MainActivity extends AppCompatActivity {
    private String user;
    private String pass;
    EditText tbuser,tbpass;
    Button btn_signin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tbuser= (EditText) findViewById(R.id.textboxuser);
        tbpass= (EditText) findViewById(R.id.textboxpass);
        btn_signin=(Button) findViewById(R.id.btn_signin);

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                user=tbuser.getText().toString();
                pass=tbpass.getText().toString();
                if ((user.equalsIgnoreCase("admin")) && pass.equalsIgnoreCase("admin"))
                {
                    Intent in=new Intent(MainActivity.this,MonhocList.class);
                    startActivity(in);
                }
                else
                    Toast.makeText(MainActivity.this,"Tên or pass sai",Toast.LENGTH_LONG).show();

            }
        });


    }
}