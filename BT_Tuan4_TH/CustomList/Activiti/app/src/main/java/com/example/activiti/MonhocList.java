package com.example.activiti;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MonhocList extends Activity {
    ListView lvMonHoc;
    ArrayList<Monhoc> arrayMonhoc;
    MonhocAdapter adapter;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_monhoc);
        anhxa();
        adapter=new MonhocAdapter(this,R.layout.monhoc,arrayMonhoc);
        lvMonHoc.setAdapter(adapter);
    }
    private  void anhxa(){
        lvMonHoc =(ListView) findViewById(R.id.listview_monhoc);
        arrayMonhoc = new ArrayList<>();
        arrayMonhoc.add(new Monhoc("C++","Số tín chỉ là 3",R.drawable.ch));
        arrayMonhoc.add(new Monhoc("C#","Số tín chỉ là 3",R.drawable.cs));
        arrayMonhoc.add(new Monhoc("Web","Số tín chỉ là 3",R.drawable.html));
        arrayMonhoc.add(new Monhoc("JavaScript","Số tín chỉ là 3",R.drawable.js));
        arrayMonhoc.add(new Monhoc("C++","Số tín chỉ là 3",R.drawable.ch));
        arrayMonhoc.add(new Monhoc("C#","Số tín chỉ là 3",R.drawable.cs));
        arrayMonhoc.add(new Monhoc("Web","Số tín chỉ là 3",R.drawable.html));
        arrayMonhoc.add(new Monhoc("JavaScript","Số tín chỉ là 3",R.drawable.js));
    }
}
