<?php
    $conn = mysqli_connect('localhost', 'root', '', 'testkt');
    if (mysqli_connect_errno() !== 0) {
        die("Error: Could not connect to the database. An error " . mysqli_connect_error() . " ocurred.");
    }
    mysqli_set_charset($conn, 'utf8');
    //select databases
    $sql = "SELECT * FROM news";
    $datas = $conn->query($sql);
    $newslist = array();
    while ($news = $datas->fetch_object()) {
        array_push($newslist, new news(
            $news->idNews,
            $news->picture,
            $news->title,
            $news->note
        ));
    }
    echo json_encode($newslist);
    //tạo class huong doi tuong
    class news
    {
        function News($id, $picture, $title, $note)
        {
            $this->id = $id;
            $this->picture = $picture;
            $this->title = $title;
            $this->note = $note;
        }
    }
?>
