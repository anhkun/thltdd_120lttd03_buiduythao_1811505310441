-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 14, 2020 lúc 01:18 PM
-- Phiên bản máy phục vụ: 10.4.13-MariaDB
-- Phiên bản PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `passiocoffee`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `idNews` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`idNews`, `picture`, `title`, `note`) VALUES
(1, 'https://scontent.fdad7-1.fna.fbcdn.net/v/t1.0-9/123021077_4880323001992686_7562951765303872398_o.jpg?_nc_cat=106&ccb=2&_nc_sid=cdbe9c&_nc_ohc=SnfDFj1OO48AX-L2q-Z&_nc_ht=scontent.fdad7-1.fna&oh=af8efb7f6218e7c6ea1ec818c365c707&oe=5FE6B610', 'PASSIO BARISTA CHAMPIONSHIP 2020 ĐÃ CHÍNH THỨC KHỞI TRANH', 'Là một trong những sự kiện thường niên quan trọng nhất của Passio, đấu trường \"Passio Barista Championship 2020\" với đã chính thức khởi động vào sáng ngày 31/10/2020. \r\nBuổi lễ khai mạc đã diễn ra trong không khí ấm cúng, sôi động với sự hiện diện của Ban giám đốc, các anh/chị từ các phòng ban và đặc biệt là gần 200 nhân viên, cửa hàng trưởng đến từ tất cả các cửa hàng trên toàn hệ thống Passio Coffee. \r\nPassio Barista Championship là hoạt động nằm trong chiến lược phát triển con người mà Passio theo đuổi trong những năm qua, nhằm tạo ra sân chơi để các nhân viên có cơ hội được giao lưu học hỏi và nâng cao tay nghề, từ đó nâng cao chất lượng phục vụ khách hàng. \r\nVới cơ cấu giải thưởng hấp dẫn, tổng giá trị lên đến 100 triệu đồng, Passio Barista Championship 2020 sẽ là nơi ươm mầm tài năng và tạo ra đội ngũ barista chuyên nghiệp phục vụ cho sứ mệnh phục vụ những ly cà phê chất lượng chuẩn Ý đúng gu Việt Nam đến với mọi khách hàng.'),
(2, 'https://scontent.fdad7-1.fna.fbcdn.net/v/t1.0-9/121335576_4792729764085344_9014545556304538852_o.jpg?_nc_cat=104&ccb=2&_nc_sid=730e14&_nc_ohc=uxniCUJfFDsAX_80_a7&_nc_ht=scontent.fdad7-1.fna&oh=1c962d0692c18f9e2098fa19f36f3da7&oe=5FE70EA9', '[GRAND OPENING] PASSIO 644 NGUYỄN THỊ ĐỊNH, QUẬN 2', 'Cửa hàng cà phê Kiosk hoàn toàn mới sẽ có mặt tại Q.2 vào 14/10/2020. Nhanh chân tới cửa hàng để tận hưởng nhiều ưu đãi cực hấp dẫn!\r\nFan Q.2 ơi, Passio sắp có thêm cửa hàng mới tại địa chỉ 644 Nguyễn Thị Định, P. Thạnh Mỹ Lợi, Q.2, TP.HCM. Đây là cửa hàng được xây dựng theo mô hình Cà phê Kiosk mà Passio giới thiệu ra thị trường trong thời gian qua. Cửa hàng mới sẽ mang đến sự tiện lợi tối đa cho khách hàng với tốc độ phục vụ sản phẩm nhanh chóng, nhưng vẫn đảm bảo chất lượng sản phẩm & dịch vụ. Cho dù bận rộn thế nào, bạn vẫn có thể thưởng thức ly cà phê thơm ngon hoặc những món nước yêu thích tại Passio. \r\nTrong tháng khai trương, Passio 644 Nguyễn Thị Định Q.2 dành tặng khách hàng chương trình ưu đãi cực kì hấp dẫn: \r\n Ưu đãi 19.000đ cà phê Espresso đá/ sữa đá (size M) từ 6h - 10h\r\nMua 1 tặng 1 từ 18h - 23h mỗi ngày. Khi mua 01 ly size L các món Latte đá, Passio Choco, Passion Chill, Chanh Tuyết, Cookie\'n cream bạn sẽ nhận ngay 01 ly size M bất kỳ thuộc các món trên. Sản phẩm được tặng có giá thấp hơn sản phẩm mua.\r\n Còn chờ gì nữa, đặt lịch hẹn đến cửa hàng mới Passio Coffee - 644 Nguyễn Thị Định, P. Thạnh Mỹ Lợi, Quận 2 bạn nhé! \r\nLưu ý: Chương trình không áp dụng đồng thời các chương trình khuyến mãi khác, Ưu đãi thành viên, Giao hàng.'),
(3, 'https://scontent.fdad7-1.fna.fbcdn.net/v/t1.0-9/120262372_4734813353210319_6082993324543341911_n.jpg?_nc_cat=108&ccb=2&_nc_sid=730e14&_nc_ohc=gubr_tD0BooAX-Pu6jq&_nc_ht=scontent.fdad7-1.fna&oh=5098ef70503b63e9391cbe7100d93769&oe=5FE73A3B', 'PASSIO 19 ĐÀO TRINH NHẤT, THỦ ĐỨC, TP.HCM', 'Đêm nhạc cực “chill” theo từng giai điệu sôi động được thể hiện bởi Âu Phong và Bảo Trân - 2 giọng ca hiện tượng đầy nội lực đã từng được MC Trấn Thành, và nhạc sĩ Only C ngợi khen. \r\nVào lúc 19h ngày 02 và 03/10, tại Passio 19 Đào Trinh Nhất – một không gian hoàn toàn mới, sẽ còn nhộn nhịp hơn với sự biểu diễn bởi MVLAND BAND, cùng sự góp giọng của 2 giọng ca hiện tượng :\r\n- Âu Phong : Giọng ca đầy cá tính đã khiến Only C “gục ngã” trong chương trình Giọng Ải Giọng Ai\r\n- Bảo Trân : Khiến Trấn Thành và giám khảo đứng ngồi không yên với giọng hát đầy cảm xúc khi góp mặt trong chương trình Hát mãi ước mơ\r\nCòn chờ gì nữa, đặt lịch hẹn đến cửa hàng mới Passio Coffee - 19 Đào Trinh Nhất, Q. Thủ Đức bạn nhé!'),
(4, 'https://scontent.fdad7-1.fna.fbcdn.net/v/t1.0-9/118890506_4619047498120239_3898381296638929040_o.jpg?_nc_cat=103&ccb=2&_nc_sid=730e14&_nc_ohc=QwPVEAFxaSwAX_Vl5Ya&_nc_ht=scontent.fdad7-1.fna&oh=781c8b3ca29adc9502d0fb10191297de&oe=5FE5C2CC', 'ƯU ĐÃI XỊN SÒ - DEAL ĐỈNH MÙA HÈ VẪN CÒN ÁP DỤNG ĐẾN 15/09', 'Bạn đã thưởng thức các thức uống mới (Chanh Xoã, Chanh Chill và Yo-crush) trong series \"Xõa xịn sò\" từ nhà Passio Coffee chưa? \r\nĐừng quên chương trình ưu đãi chỉ 89k cho 02 thức uống bất kì trong dòng sản phẩm \"Xõa xịn sò\" vẫn đang diễn ra đến ngày 15/09. \r\nĐặt ngay lịch hẹn và đến các cửa hàng của Passio Coffee để thưởng thức bạn nhé \r\n-------------------------\r\n Lưu ý: chương trình áp dụng từ 14h hằng ngày, đến hết ngày 15/9. \r\n Không áp dụng đồng thời các chương trình khuyến mãi khác, ưu đãi thành viên, giao hàng.\r\n#PassioCoffeeVietnam #xõa_cùng_Passio'),
(5, 'https://scontent.fdad7-1.fna.fbcdn.net/v/t1.0-9/118677728_4593971997294456_5548930901742008682_o.jpg?_nc_cat=103&ccb=2&_nc_sid=730e14&_nc_ohc=UL4hbFaAOZEAX88JIgM&_nc_ht=scontent.fdad7-1.fna&oh=48d07fcf0861bafe6bc4355b65623283&oe=5FE71B91', 'NHẬN QUÀ XỊN TỪ PASSIO COFFEE CHƯA BAO GIỜ DỄ ĐẾN THẾ', 'Tặng 100 TÚI VẢI CỰC COOL cho hoá đơn từ 99K chỉ duy nhất ngày mai! \r\nĐừng quên cái hẹn vào 8h sáng ngày mai (05/9) tại Passio Coffee 47 Trần Cao Vân nha cả nhà ơi! \r\nBạn sẽ nhận ngay 01 TÚI THỜI TRANG cực chất khi:\r\n=>Mua hoá đơn từ 99K trở lên\r\n=>Check-in công khai trên Facebook cá nhân \r\nNote lịch đến Passio để nhận ngay túi xịn nào bạn ơi !!\r\n----------------------------\r\nChương trình diễn ra chỉ trong ngày 05/9 (khung giờ 8h đến 10h30)\r\nKhông áp dụng đồng thời các chương trình khuyến mãi khác, ưu đãi thành viên, giao hàng.');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`idNews`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `idNews` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
