package com.example.btthktralan4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsAdapter extends BaseAdapter {
    private Context context;
    private List<News> list;
    private int layout;

    public NewsAdapter(Context context, List<News> list, int layout) {
        this.context = context;
        this.list = list;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private class ViewHolder{
        ImageView image;
        TextView txtTitle,txtContent;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view==null){
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holder=new ViewHolder();
            holder.image=(ImageView) view.findViewById(R.id.line_image);
            holder.txtTitle=(TextView) view.findViewById(R.id.line_title);
            holder.txtContent=(TextView) view.findViewById(R.id.line_content);
            view.setTag(holder);
        }
        else
            holder= (ViewHolder) view.getTag();
        News news=list.get(i);
        Picasso.with(context).load(news.getImage())
                .placeholder(R.drawable.anhdaidien)
                .error(R.drawable.ic_baseline_card_giftcard_24)
                .into(holder.image);
        holder.txtTitle.setText(news.getTitle());
        holder.txtContent.setText(news.getContent());
        return view;
    }
}
